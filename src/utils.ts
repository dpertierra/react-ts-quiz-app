import BGImage from "./images/background1.jpg";
import BGImage2 from "./images/background2.jpg";
import BGImage3 from "./images/background3.jpg";
export const shuffleArray = (array: any[]) =>
  [...array].sort(() => Math.random() - 0.5);
export const getBackground = () => {
  switch (Math.floor(Math.random() * (4 - 1)) + 1) {
    case 1:
      return BGImage;
    case 2:
      return BGImage2;
    case 3:
      return BGImage3;
  }
};
