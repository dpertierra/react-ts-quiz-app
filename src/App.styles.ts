import styled, { createGlobalStyle } from "styled-components";
import { getBackground } from "./utils";

export const GlobalStyle = createGlobalStyle`
  html{
    height: 100%;
  }
  
  body{
    background-image: url(${getBackground()});
    background-size: cover;
    margin: 0;
    padding: 0 20px;
    display: flex;
    justify-content: center;
  }
  
  *{
    box-sizing: border-box;
    font-family: 'Catamaran', sans-serif;
  }
  
  
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  > p {
    color: #fff;
  }

  .score {
    color: #fff;
    font-size: 2rem;
    margin: 0;
  }

  h1 {
    font-family: Fascinate Inline, sans-serif;
    background-image: linear-gradient(180deg, #fff, #1c4ef3);
    font-weight: 400;
    background-size: 100%;
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    -moz-background-clip: text;
    -moz-text-fill-color: transparent;
    filter: drop-shadow(2px 2px #5557ec);
    font-size: 9rem;
    text-align: center;
    margin: 20px;
  }

  .start,
  .next {
    cursor: pointer;
    background: linear-gradient(180deg, #fff, #01b9be);
    border: 2px solid #026b7c;
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    height: 40px;
    margin: 20px 0;
    padding: 0 40px;
  }
  .start {
    max-width: 200px;
  }
`;
